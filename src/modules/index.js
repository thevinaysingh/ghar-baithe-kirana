/**
* @providesModule src/modules
*/

import LoginScreen from '../modules/auth/screens/LoginScreen';
import SignupScreen from '../modules/auth/screens/SignupScreen';
import HomeScreen from '../modules/home';
import CategoryScreen from '../modules/home/screens/CategoryItems';
import SubCategoryScreen from '../modules/home/screens/SubCategoryItems';
import MyOrderScreen from '../modules/home/screens/MyOrder';
import MyCartScreen from '../modules/home/screens/MyCart';
import MyKiranaScreen from '../modules/home/screens/MyKirana';
import ContactUsScreen from '../modules/contactUs';
import FeedbackScreen from '../modules/feedback';
import TermsAndConditionsScreen from '../modules/termsAndConditions';
import AboutUsScreen from '../modules/home/screens/AboutUs';

// Screens for admin page
import AdminHomeScreen from '../modules/admin-home/screen/AdminHome';
import CategoryForm from '../modules/admin-home/screen/CategoryForm';

export {
  LoginScreen,
  SignupScreen,
  HomeScreen,
  CategoryScreen,
  SubCategoryScreen,
  ContactUsScreen,
  FeedbackScreen,
  TermsAndConditionsScreen,
  MyOrderScreen,
  MyCartScreen,
  MyKiranaScreen,
  AboutUsScreen,
  AdminHomeScreen,
  CategoryForm,
};
