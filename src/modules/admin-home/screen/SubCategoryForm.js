import React, { Component } from 'react';
import {
  Alert,
  ActivityIndicator,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import { Container, Content, Card, Item, Input, Button, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Colors, dimensions, labelStyles, containerStyles } from '../../../themes';
import { Header, StatusBar } from '../../../components';
import {
  linkState,
  isEmailValid,
  isAddressValid,
  isPhoneValid,
  assignRef,
  focusOnNext,
} from '../../../utils';

const styles = {
  iconStyle: {
    color: Colors.themeIconColor,
    marginRight: 5,
  },
  iconEyeStyle: {
    color: Colors.whiteIconColor,
    width: 30,
    height: 30,
    alignSelf: 'center',
    borderRadius: 5,
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: Colors.primaryBgColor,
  },
};

class SubCategoryForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      subName: '',
      create: true,   
      // title: account.app_name,
      // webUrl: account.web_url,
      // userId: account.username,
      // password: account.password,
      // linkedMob: account.linked_mob,
      // linkedEmail: account.linked_email,
      // objective: account.objective,
      // showPassword: false,
      // isLoading: false,
    };
  }

  // handleSubmit() {
  //   const {
  //     title,
  //     webUrl,
  //     userId,
  //     password,
  //     linkedMob,
  //     linkedEmail,
  //     objective,
  //   } = this.state;
  //   if (linkedEmail.length > 0 || linkedMob.length > 0) {
  //     if (!isEmailValid(linkedEmail)) {
  //       Alert.alert('Input Error', 'Linked email is not valid');
  //       return;
  //     }
  //     Alert.alert('Input Error', 'Linked phone is not valid');
  //     return;
  //   }
  //   const digitalAccount = {
  //     app_name: title,
  //     web_url: webUrl,
  //     username: userId,
  //     password,
  //     linked_email: linkedEmail === '' ? 'unset' : linkedEmail,
  //     linked_mob: linkedMob === '' ? 'unset' : linkedMob,
  //     objective: objective === '' ? 'unset' : linkedMob,
  //     type: 'Others',
  //     date: new Date().getDate().toString(),
  //   };
  //   networkConnectivity().then(() => {
  //     if (this.props.create) {
  //       this.setState({ isLoading: true });
  //       FirebaseManager.addAccount(digitalAccount)
  //         .then(() => {
  //           this.setState({ isLoading: false });
  //           ToastAndroid.show('Successfully added!', ToastAndroid.LONG);
  //           Actions.homeScreen({ type: 'reset' });
  //         }).catch((error) => {
  //           this.setState({ isLoading: false });
  //           Alert.alert('Error', `${error}`);
  //         });
  //     } else {
  //       FirebaseManager.updateAccount(digitalAccount, this.props.account.key)
  //       .then(() => {
  //         this.setState({ isLoading: false });
  //         ToastAndroid.show('Successfully updated!', ToastAndroid.LONG);
  //         Actions.homeScreen({ type: 'reset' });
  //       }).catch((error) => {
  //         this.setState({ isLoading: false });
  //         Alert.alert('Error', `${error}`);
  //       });
  //     }
  //   }).catch((error) => {
  //     Alert.alert('Network Error', `${error}`);
  //   });
  // }

  // isSubmitDisabled = () => {
  //   const {
  //     title,
  //     webUrl,
  //     userId,
  //     password,
  //   } = this.state;
  //   if (!isAddressValid(title) || !isAddressValid(webUrl)
  //     || !isAddressValid(userId) || !isAddressValid(password)) {
  //     return true;
  //   }
  //   return false;
  // }

  render() {
    const isSubmitDisabled = false;
    const { create } = this.state;
    return (
      <Container style={containerStyles.defaultContainer}>
        {!create && <StatusBar />}
        {!create && <Header
          title={'Edit Sub Category'}
          onPressleftIcon={() => Actions.adminHomeScreen({ type: 'reset' })}
          showMenu={false}
        />}
        <Content contentContainerStyle={{ padding: dimensions.defaultDimension }}>
          <Card style={{ padding: dimensions.defaultDimension }}>
            <Item
              style={{
                marginBottom: dimensions.smallDimension,
                marginTop: -dimensions.smallDimension,
              }}
            >
              <Icon style={styles.iconStyle} size={20} name='briefcase' />
              <Input
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'Sub Category name'}
                returnKeyType={'next'}
                onSubmitEditing={() => focusOnNext(this, 'subNameInput')}
                {...linkState(this, 'name')}
              />
            </Item>
            <Item style={{ marginVertical: dimensions.smallDimension }}>
              <Icon style={styles.iconStyle} size={20} name='briefcase' />
              <Input
                ref={ref => assignRef(this, 'subNameInput', ref)}
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'Subname'}
                returnKeyType={'done'}
                {...linkState(this, 'subName')}
              />
            </Item>
          </Card>
        </Content>
      </Container>
    );
  }
}

SubCategoryForm.propTypes = {
  account: PropTypes.any,
  create: PropTypes.bool,
};

SubCategoryForm.defaultProps = {
  account: {},
  create: true,
};

const mapStateToProps = state => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(SubCategoryForm);
