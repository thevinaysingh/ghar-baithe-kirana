import React from 'react';
import Swiper from 'react-native-swiper';
import PropTypes from 'prop-types';
import {
  Text,
  StyleSheet,
  View,
  Image,
} from 'react-native';
import { Colors, dimensions } from '../../../themes';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    borderWidth: 1,
    borderBottomColor: Colors.blackIconColor,
    backgroundColor: Colors.defaultGreyColor,
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    height: 200,
    width: dimensions.getViewportWidth(),
  },
});

const ImageSwiper = props => (
  <View style={{ height: 200, alignSelf: 'stretch' }}>
    <Swiper
      style={styles.wrapper}
      showsButtons={Boolean(true)}
    >
      <View style={styles.slide}>
        <Image
          style={styles.imageStyle}
          source={{ uri: props.images[0] }}
        />
        {!props.images[0] && <Text>No image</Text>}
      </View>
      <View style={styles.slide}>
        <Image
          style={styles.imageStyle}
          source={{ uri: props.images[1] }}
        />
        {!props.images[1] && <Text>No image</Text>}
      </View>
      <View style={styles.slide}>
        <Image
          style={styles.imageStyle}
          source={{ uri: props.images[2] }}
        />
        {!props.images[2] && <Text>No image</Text>}
      </View>
    </Swiper>
  </View>
);

ImageSwiper.propTypes = {
  images: PropTypes.any,
};

ImageSwiper.defaultProps = {
  images: [],
};

export default ImageSwiper;
