import React, { Component } from 'react';
import { Provider } from 'react-redux';
import runRootSaga from 'src/rootSaga';
import { store } from 'src/store';
import Router from 'src/router';
import { FirebaseConfig } from './firebase';

export default class App extends Component {
  constructor() {
    super();
    runRootSaga();
    FirebaseConfig.initialise();
  }

  render() {
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}
