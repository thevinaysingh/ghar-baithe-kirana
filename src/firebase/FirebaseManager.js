import * as firebase from 'firebase';

export class FirebaseManager {
  static uid = '';
  static profile = {};
  static items = [];
  // push().getKey();

  static getAuthRef = () => {
    return firebase.auth();
  }
  static getDBRef = () => {
    return firebase.database();
  }
  static getStorageRef = () => {
    return firebase.storage();
  }

  static getUserRef = (uid) => {
    FirebaseManager.getDBRef().ref('users/'+ uid);
  };
  static geCategoryRef = (cid) => {
    FirebaseManager.getDBRef().ref('categories/'+ cid);
  };
  static getSubcategoryRef = (sid) => {
    FirebaseManager.getDBRef().ref('sub_categories/'+ sid);
  };

  static getUserProfileRef = (uid) => {
    FirebaseManager.getDBRef().ref('users/'+ uid +'/profile');
  };

  static getUserItemRef = (uid) => {
    FirebaseManager.getDBRef().ref('users/'+ uid +'/items');
  };

  /* TODO: */
  static updateProfile = (profile) => {};

  /* TODO: */
  static setProfile = (profile) => {};

  /* TODO: */
  static setAvatar = (avatar) => {};

  /* TODO: */
  static updateAvatar = (avatar) => {};
}