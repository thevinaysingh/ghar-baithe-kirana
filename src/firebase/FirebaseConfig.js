import * as firebase from 'firebase';

export class FirebaseConfig {
  /**
   * Initialise Firebase
   */
  static initialise() {
    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: 'AIzaSyB_tBJErPOhvCeml1G-dIIPa3IfavdR4mw',
        authDomain: 'ghar-baithe-kirana.firebaseapp.com',
        databaseURL: 'https://ghar-baithe-kirana.firebaseio.com',
        projectId: 'ghar-baithe-kirana',
        storageBucket: 'ghar-baithe-kirana.appspot.com',
        messagingSenderId: '877364925563',
      });
    }
  }
}
